# Simple Price Calculator

- Backend : Java Spring Boot, Gradle
- Frontend : Angular
- Database : Postgress

## Instructions to run application
- Clone the repository
- Navigate to project folder

### Configure Database (Postgress)
- cd ./environments
- docker-compose-build
- docker-compose-up

### Backend (Java)
- cd ./
- Run the commands , gradle clean build bootRun
- default host - localhost:9050

### Insert default products
- Use (http://localhost:9050/api/save) POST API call by Postman.
- Sample requests
```json
{
	"title":"Horseshoe",
	"unitPrice":"0",
	"cartonPrice":"825",
	"unitsPerCarton":"5"
}
```
```json
{
	"title":"Penguin-ears",
	"unitPrice":"0",
	"cartonPrice":"175",
	"unitsPerCarton":"20"
}
```

### Frontend (Angular)
- cd ./frontend
- npm install
- ng serve
- default host - localhost:4200

### Screenshots
![Alt text](https://i.ibb.co/Yk6pNmN/Screenshot-353.png "Price List")
![Alt text](https://i.ibb.co/LtvhNpk/Screenshot-354.png "Price Calculator")