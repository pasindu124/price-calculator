import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.css']
})
export class PriceListComponent implements OnInit {
  products: any;
  productPriceMap: any;

  constructor(private productService:ProductService) { }

  ngOnInit() {
    this.retrieveProducts();
    this.retrieveProductPriceMap();
  }

    retrieveProducts() {
        this.productService.getAllProducts().subscribe(
            data => {
                this.products = data;
            },
            error => {
                console.log(error);
            });
    }

    private retrieveProductPriceMap() {
        this.productService.getProductPriceMap().subscribe(
            data => {
                this.productPriceMap = data;
                console.log(data);
            },
            error => {
                console.log(error);
            });
    }

    counter(i: number) {
        // @ts-ignore
        return new Array(i);
    }

}
