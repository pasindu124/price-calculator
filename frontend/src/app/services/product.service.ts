import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:9050/api';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getAllProducts(): Observable<any> {
    let endPoint =  "getProducts"
    return this.http.get(`${baseUrl}/${endPoint}`);
  }

  getProductPriceMap(): Observable<any> {
    let endPoint =  "getProductPriceMap"
    return this.http.get(`${baseUrl}/${endPoint}`);
  }

  getProductPrice(id: string, numberOfCartons: string, numberOfUnits: string): Observable<any> {
    let endPoint =  "getProductPrice"
    return this.http.post(`${baseUrl}/${endPoint}`,
        {"productId": id, "numberOfCartons": numberOfCartons, "numberOfUnits": numberOfUnits});
  }
}
