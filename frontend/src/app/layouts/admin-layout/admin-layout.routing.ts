import { Routes } from '@angular/router';

import { PriceCalculatorComponent } from '../../price-calculator/price-calculator.component';
import { PriceListComponent } from '../../product-prices/price-list.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'price-calculator',   component: PriceCalculatorComponent },
    { path: 'price-list',     component: PriceListComponent },
];
