import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";

@Component({
  selector: 'app-price-calculator',
  templateUrl: './price-calculator.component.html',
  styleUrls: ['./price-calculator.component.css']
})
export class PriceCalculatorComponent implements OnInit {
  private products: any;
  cartonQuantity: any = 0;
  unitsQuantity: any = 0;
  totalPrice: any = 0;
  selectedProduct: any = "3";
  public defaultProduct: any;
  buttonDisabled: boolean = false;

  constructor(private productService:ProductService) { }

  ngOnInit() {
    this.retrieveProducts();
  }

  retrieveProducts() {
    this.productService.getAllProducts().subscribe(
        data => {
          this.products = data;
          this.defaultProduct = data[1].id.toString();
        },
        error => {
          console.log(error);
        });
  }

  submit() {
    this.buttonDisabled = true;
    this.productService.getProductPrice(this.selectedProduct, this.cartonQuantity, this.unitsQuantity).subscribe(
        data => {
          this.totalPrice = data.totalPrice;
          this.buttonDisabled = false;
        },
        error => {
          console.log(error);
        });
  }
}
