package com.pasindu.price.logic;

import com.pasindu.price.dto.GetProductPriceReqDTO;
import com.pasindu.price.dto.GetProductPriceResDTO;
import com.pasindu.price.dto.ProductDTO;
import com.pasindu.price.dto.ProductPriceDTO;
import com.pasindu.price.model.Product;
import com.pasindu.price.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductLogicTest {
	@InjectMocks
	ProductLogic productLogic;

	@Mock ProductRepository productRepository;

	@Mock ModelMapper modelMapper;

	Product product;

	List<Product> productList = new ArrayList<>();


	@BeforeEach
	void setUp() {
		product = new Product();
		product.setId(3L);
		product.setTitle("Penguin-ears");
		product.setCartonPrice("175");
		product.setUnitsPerCarton("20");
		product.setUnitPrice("0");

		productList.add(product);

	}
	@Test
	public void  getAllProductsTest(){
		productLogic.getAllProducts();
		verify(productRepository, times(1)).findAll();
	}

	@Test
	public void  saveProductTest(){
		ProductDTO productDTO = new ProductDTO();
		Product product = modelMapper.map(productDTO, Product.class);
		productLogic.saveProduct(productDTO);
		verify(productRepository, times(1)).save(product);
	}

	@Test
	public void getProductPriceDetailsTest(){
		when(productRepository.findAll()).thenReturn(productList);
		List<ProductPriceDTO> productPriceDTOS = productLogic.getProductPriceDetails();
		Assertions.assertEquals(1,productPriceDTOS.size());
		Assertions.assertEquals(11.375,productPriceDTOS.get(0).getPriceMap().get(1));
		Assertions.assertEquals(463.75,productPriceDTOS.get(0).getPriceMap().get(50));
		verify(productRepository, times(1)).findAll();
	}

	@Test
	public void getProductPriceTest(){

		when(productRepository.findById(3L)).thenReturn(Optional.of(product));
		GetProductPriceReqDTO productPriceReq = new GetProductPriceReqDTO();
		productPriceReq.setProductId(3L);
		productPriceReq.setNumberOfCartons(2);
		productPriceReq.setNumberOfUnits(2);
		GetProductPriceResDTO getProductPriceReqDTO = productLogic.getProductPrice(productPriceReq);
		verify(productRepository, times(1)).findById(productPriceReq.getProductId());
		Assertions.assertEquals(372.75,getProductPriceReqDTO.getTotalPrice());
	}
}
