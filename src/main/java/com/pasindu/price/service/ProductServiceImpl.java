package com.pasindu.price.service;

import com.pasindu.price.dto.GetProductPriceReqDTO;
import com.pasindu.price.dto.GetProductPriceResDTO;
import com.pasindu.price.dto.ProductDTO;
import com.pasindu.price.dto.ProductPriceDTO;
import com.pasindu.price.logic.ProductLogic;
import com.pasindu.price.model.Product;
import com.pasindu.price.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductLogic productLogic;

	@Override
	public List<ProductDTO> getAllProducts() {
		return productLogic.getAllProducts();
	}

	@Override
	public void saveProduct(ProductDTO productDTO) {
		productLogic.saveProduct(productDTO);
	}

	@Override
	public List<ProductPriceDTO> getProductPriceDetails() {
		return productLogic.getProductPriceDetails();
	}

	@Override
	public GetProductPriceResDTO getProductPrice(GetProductPriceReqDTO req) {
		return productLogic.getProductPrice(req);
	}
}
