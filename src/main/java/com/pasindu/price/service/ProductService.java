package com.pasindu.price.service;

import com.pasindu.price.dto.GetProductPriceReqDTO;
import com.pasindu.price.dto.GetProductPriceResDTO;
import com.pasindu.price.dto.ProductDTO;
import com.pasindu.price.dto.ProductPriceDTO;

import java.util.List;

public interface ProductService {

	List<ProductDTO> getAllProducts();

	void saveProduct(ProductDTO productDTO);

	List<ProductPriceDTO> getProductPriceDetails();

	GetProductPriceResDTO getProductPrice(GetProductPriceReqDTO req);
}
