package com.pasindu.price.logic;

import com.pasindu.price.dto.GetProductPriceReqDTO;
import com.pasindu.price.dto.GetProductPriceResDTO;
import com.pasindu.price.dto.ProductDTO;
import com.pasindu.price.dto.ProductPriceDTO;
import com.pasindu.price.model.Product;
import com.pasindu.price.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ProductLogic {
	@Autowired ProductRepository productRepository;
	@Autowired ModelMapper modelMapper;

	public List<ProductDTO> getAllProducts() {
		List<Product> products = productRepository.findAll();
		List<ProductDTO> productDTOList = products.stream().map(p -> modelMapper.map(p, ProductDTO.class))
				.collect(Collectors.toList());
		return productDTOList;
	}

	public void saveProduct(ProductDTO productDTO) {
		Product product = modelMapper.map(productDTO, Product.class);
		productRepository.save(product);
	}

	public List<ProductPriceDTO> getProductPriceDetails() {
		List<ProductPriceDTO> productPrices = new ArrayList<>();

		List<Product> products = productRepository.findAll();
		for (Product product: products) {
			ProductPriceDTO productPrice = new ProductPriceDTO();
			Map<Integer, Double> priceMap = new HashMap<>();
			for (int i = 1 ; i <= 50; i++) {
				priceMap.put(i,calculatePrice(product, i));

			}

			productPrice.setProductTitle(product.getTitle());
			productPrice.setPriceMap(priceMap);
			productPrices.add(productPrice);
		}

		return productPrices;
	}

	private Double calculatePrice(Product product, int quantity) {
		int unitsPerCarton  = Integer.parseInt(product.getUnitsPerCarton());
		double cartonPrice = Double.parseDouble(product.getCartonPrice());

		int numberOfCartons = quantity / unitsPerCarton;
		int numberOfUnits = quantity % unitsPerCarton;

		if (numberOfCartons > 2) {
			cartonPrice = cartonPrice - cartonPrice*0.1;
		}

		double increseCartonPrice = cartonPrice + cartonPrice*0.3;
		double unitPrice = increseCartonPrice / unitsPerCarton;

		return getTotalPrice(cartonPrice, unitPrice, numberOfCartons, numberOfUnits);
	}

	private double getTotalPrice(double cartonPrice, double unitPrice, int numberOfCartons, int numberOfUnits) {
		double priceForCartons = numberOfCartons* cartonPrice;
		double priceForUnits = numberOfUnits * unitPrice;

		double totalPrice = priceForCartons + priceForUnits;
		return totalPrice;
	}

	public GetProductPriceResDTO getProductPrice(GetProductPriceReqDTO req) {
		GetProductPriceResDTO resDTO = new GetProductPriceResDTO();
		Optional<Product> productOptional = productRepository.findById(req.getProductId());

		if (productOptional != null) {
			Product product = productOptional.get();
			int  numberOfUnits = req.getNumberOfUnits() + (req.getNumberOfCartons()* Integer.parseInt(product.getUnitsPerCarton()));
			double totalPrice = calculatePrice(product, numberOfUnits);
			resDTO.setTotalPrice(totalPrice);
		}
		return resDTO;
	}
}
