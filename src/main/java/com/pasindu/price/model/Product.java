package com.pasindu.price.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "unit_price")
	private String unitPrice;

	@Column(name = "carton_price")
	private String cartonPrice;

	@Column(name = "units_per_carton")
	private String unitsPerCarton;
}
