package com.pasindu.price.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetProductPriceResDTO {

	private Double totalPrice;
}
