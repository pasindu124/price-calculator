package com.pasindu.price.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDTO {

	private Long id;

	private String title;

	private String unitPrice;

	private String cartonPrice;

	private String unitsPerCarton;

}
