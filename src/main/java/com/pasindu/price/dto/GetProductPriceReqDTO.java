package com.pasindu.price.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetProductPriceReqDTO {

	private Long productId;

	private Integer numberOfCartons;

	private Integer numberOfUnits;

}
