package com.pasindu.price.dto;

import java.util.Map;

public class ProductPriceDTO {

	private String productTitle;

	private Map<Integer, Double> priceMap;

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public Map<Integer, Double> getPriceMap() {
		return priceMap;
	}

	public void setPriceMap(Map<Integer, Double> priceMap) {
		this.priceMap = priceMap;
	}
}
