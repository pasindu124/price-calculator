package com.pasindu.price.controller;

import com.pasindu.price.dto.GetProductPriceReqDTO;
import com.pasindu.price.dto.GetProductPriceResDTO;
import com.pasindu.price.dto.ProductDTO;
import com.pasindu.price.dto.ProductPriceDTO;
import com.pasindu.price.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping(path = "api")
public class ProductController {

	@Autowired
	ProductService productService;

	@GetMapping("/getProducts")
	public List<ProductDTO> getAllProducts() {
		return productService.getAllProducts();
	}

	@PostMapping("/save")
	public ProductDTO SaveProduct(@RequestBody ProductDTO productDTO){
		productService.saveProduct(productDTO);
		return productDTO;
	}

	@GetMapping("/getProductPriceMap")
	public List<ProductPriceDTO> getProductPriceDetails() {
		return productService.getProductPriceDetails();
	}

	@PostMapping("/getProductPrice")
	public GetProductPriceResDTO getProductPrice(@RequestBody GetProductPriceReqDTO req) {
		return productService.getProductPrice(req);
	}
}
